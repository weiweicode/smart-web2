package cn.com.smart.form.enums;

import cn.com.smart.web.bean.LabelValue;
import com.mixsmart.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单列表类型
 * @author 乌草坡 2019-09-01
 * @since 1.0
 */
public enum FormListType {
    MAIN("main", "主表单"),
    SUB("sub", "子表单");

    private String value;

    private String text;

    FormListType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取表单列表类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static FormListType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        FormListType type = null;
        for(FormListType typeTmp : FormListType.values()) {
            if(typeTmp.getValue().equals(value)) {
                type = typeTmp;
                break;
            }
        }
        return type;
    }

    /**
     * 获取选项列表
     * @return 返回选项列表
     */
    public static List<LabelValue> getOptions() {
        List<LabelValue> labelValues = new ArrayList<LabelValue>();
        for(FormListType typeTmp : FormListType.values()) {
            labelValues.add(new LabelValue(typeTmp.getText(), typeTmp.getValue()));
        }
        return labelValues;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
