package cn.com.smart.form.enums;

import java.util.ArrayList;
import java.util.List;

import com.mixsmart.utils.StringUtils;

import cn.com.smart.web.bean.LabelValue;

/**
 * 定义搜索表达式类型
 * @author lmq
 *
 */
public enum SearchExpType {

    EQ("eq", "等于", "="),
    LT("lt", "小于", "<"),
    GT("gt", "大于", ">"),
    LTE("lte", "小于等于", "<="),
    GTE("gte", "大于等于", ">="),
    LIKE("like", "模糊", "like '%:value%'"),
    LEFT_LIKE("left_like", "左模糊", "like '%:value'"),
    RIGHT_LIKE("right_like", "右模糊", "like ':value%'");
    
    private String value;
    
    private String text;

    private String exp;

    private SearchExpType(String value, String text, String exp) {
        this.value = value;
        this.text = text;
        this.exp = exp;
    }

    /**
     * 获取搜索表达式类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static SearchExpType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        SearchExpType type = null;
        for(SearchExpType typeTmp : SearchExpType.values()) {
            if(typeTmp.getValue().equals(value)) {
                type = typeTmp;
                break;
            }
        }
        return type;
    }
    
    /**
     * 获取选项列表
     * @return 返回选项列表
     */
    public static List<LabelValue> getOptions() {
        List<LabelValue> labelValues = new ArrayList<LabelValue>();
        for(SearchExpType typeTmp : SearchExpType.values()) {
            labelValues.add(new LabelValue(typeTmp.getText(), typeTmp.getValue()));
        }
        return labelValues;
    }
    
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }
}
